/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inherrithance;

/**
 *
 * @author a
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White",0);
        animal.speak();
        animal.walk();
            
        Dog dang = new Dog("Dang", "BlackAndWhite");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("To", "Orange");
        to.speak();
        to.walk();
        
        Dog mome = new Dog("Mome", "WhiteAndBlack");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat", "WhiteAndBlack");
        bat.speak();
        bat.walk();
        
        Cat cat = new Cat("Zero", "Orange");
        cat.speak();
        cat.walk();
        
        Duck duck = new Duck("Zom", "Yellow");
        duck.speak();
        duck.walk();
        duck.fly();
        
        Duck gabgab = new Duck("GabGab", "Yellow");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        
        
        
        System.out.println("Zom is Animal: " + (duck instanceof Animal));
        System.out.println("Zom is Duck: " + (duck instanceof Duck));
        System.out.println("Zom is cat: " + (duck instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        System.out.println("To is Animal" + (to instanceof Animal));
        System.out.println("Mome is Animal" + (mome instanceof Animal));
        System.out.println("Bat is Animal" + (bat instanceof Animal));
        System.out.println("GabGab is Animal" + (gabgab instanceof Animal));
        
        Animal[] animals = {dang, cat ,duck, to, mome, bat, gabgab};
        for(int i = 0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck zom = (Duck) animals[i];
                zom.fly();
            }
        }
        
        
    }
     
}
