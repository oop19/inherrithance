/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inherrithance;

/**
 *
 * @author a
 */
public class Cat extends Animal {

    public Cat(String name, String color) {
        super(name, color, 4);
        System.out.println("Cat created");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Cat: " + name + " walk with " + numberOfLegs + "Legs");
    }

    @Override
    public void speak() {
        super.speak();
        System.out.println("Cat: " + name + " speak < meow meow!! ");
    }
}
